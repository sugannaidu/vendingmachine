# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
	* Design and develop a vending machine which vends products based upon four (4) denominations of coins and returns coins if there is no item.
	* We require you to develop a single page application that does the following:
	* When the application loads, it loads data about all the instances from the products and Coins and visualizes this data in some way. (e.g. A table with all the data)
	* The application must have some mechanism for refreshing all the data from the API without having to reload the entire application (e.g. A refresh data button)
	* The application must have some mechanism for refreshing the data of only a section (products or coins)
	* The application should show the user some indication that it is refreshing/loading new data

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
    - Extract contents of zip file then use gradle to build & run application 
    -./gradlew clean build bootRun

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Developement workflows
##### How to run frontend & backend together
  * Update *.angular-cli.json* set "outDir": "../src/main/resources/static"
  * Run backend .... *./gradlew bootRun*
	* http://localhost:8080 & http://localhost:8080/api/hello

##### How to run frontend independently of backend
   * Update *proxy-config proxy.conf.json* to access backend rest services
        * {
            "/api": {
              "target": "http://localhost:8080",
              "secure": false
            }
          }
   * Run frontend ... *ng serve --proxy-config proxy.conf.json*
        * http://localhost:4200 & http://localhost:4200/api/hello
   * Run backend .... *./gradlew bootRun*
        * http://localhost:8080 or http://localhost:8080/api/hello

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
	- Sugan Naidu - sugan.naidu@gmail.com
* Other community or team contact


------

# Analysis
### Requirements
#### Functional Requirements
>Design and develop a vending machine which vends products based upon four (4) denominations of coins and returns coins if there is no item.

**User stories for Vending machine:.**

* As a customer when I tender an amount for an item that is equal to the cost of the item using the 4 denominations of coins & there is quantity available for selected item then I should receive the item.
* As a customer when I tender an amount for an item that is greater than the cost of the item using the 4 denominations of coins & there is quantity available for selected item then I should receive the item & change.
* As a customer when I tender an amount for an item that is less than the cost of the item using the 4 denominations of coins &  there is quantity available for selected item then I should not receive the item.
* As a customer when I tender an amount for an item that is greater or equal to the cost of the item using the 4 denominations of coins &  there is no quantity available for selected item then I should receive the tendered amount back.
* As a customer when I tender an amount for an item using the incorrect denominations of coins then I should receive the tendered amount back.
* As a customer when I tender an amount for an item using the 4 denominations of coins & then cancel the transaction then I should receive the tendered amount back.

#### Non Functional Requirements
 We require you to develop a single page application that does the following:
> When the application loads, it loads data about all the instances from the products and Coins and visualizes this data in some way. (e.g. A table with all the data).

* Product listing with Coins.

> The application must have some mechanism for refreshing all the data from the API without having to reload the entire application (e.g. A refresh data button).

* Asynchronous call to refresh all depenedent data.

> The application must have some mechanism for refreshing the data of only a section (products or coins)

* Asynchronous call to refresh products only.
* Asynchronous call to refresh coins only.


> The application should show the user some indication that it is refreshing/loading new data

* Asynchronous call to refresh data while ui displays visual indicator
*

------
# Design and Implementation
### **Steps to complete:. **

* Add git repository on BitBucket for Vending Machine.
* Create SpringBoot application - Rest Backend + Tomcat.
* Create Angular Frontend - add to SpringBoot with Gradle.
* Use JPA + H2 database (embedded or in-memory).
* Testing Backend layer.
* Testing Frontend layer.
* Flesh out domain layer.
* User stories for functional requirements and Non functional requirements.
* Performance refactoring.
* Transaction Management.
