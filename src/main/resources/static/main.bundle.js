webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "tootlbar-icon {\n  padding: 0 14px;\n}\n\n.tootlbar-spacer {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n}\n\n/* Absolute Center Spinner */\n.loading {\n  position: fixed;\n  z-index: 999;\n  height: 2em;\n  width: 2em;\n  overflow: show;\n  margin: auto;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n}\n\n/* Transparent Overlay */\n.loading:before {\n  content: '';\n  display: block;\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(0,0,0,0.3);\n}\n\n/* :not(:required) hides these rules from IE9 and below */\n.loading:not(:required) {\n  /* hide \"loading...\" text */\n  font: 0/0 a;\n  color: transparent;\n  text-shadow: none;\n  background-color: transparent;\n  border: 0;\n}\n\n.loading:not(:required):after {\n  content: '';\n  display: block;\n  font-size: 10px;\n  width: 1em;\n  height: 1em;\n  margin-top: -0.5em;\n  -webkit-animation: spinner 1500ms infinite linear;\n  animation: spinner 1500ms infinite linear;\n  border-radius: 0.5em;\n  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;\n}\n\n/* Animation */\n\n@-webkit-keyframes spinner {\n  0% {\n    -webkit-transform: rotate(0deg);\n    transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n    transform: rotate(360deg);\n  }\n}\n@keyframes spinner {\n  0% {\n    -webkit-transform: rotate(0deg);\n    transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n    transform: rotate(360deg);\n  }\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-vending-machine>\n\n</app-vending-machine>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Vending Machine';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__vending_machine_vending_machine_module__ = __webpack_require__("../../../../../src/app/vending-machine/vending-machine.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__message_message_component__ = __webpack_require__("../../../../../src/app/message/message.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__message_message_module__ = __webpack_require__("../../../../../src/app/message/message.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
            __WEBPACK_IMPORTED_MODULE_3__vending_machine_vending_machine_module__["a" /* VendingMachineModule */],
            __WEBPACK_IMPORTED_MODULE_5__message_message_module__["a" /* MessageModule */],
            __WEBPACK_IMPORTED_MODULE_6__angular_router__["a" /* RouterModule */].forRoot([
                { path: '', component: __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */] },
                { path: '**', redirectTo: '/' }
            ])
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */], __WEBPACK_IMPORTED_MODULE_4__message_message_component__["a" /* MessageComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/coins/coins.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/coins/coins.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-default\">\n  <div class=\"card-block\">\n    <!--<div class=\"card-block card-inverse\">-->\n    <div class=\"bg-info p-a-1\">\n      <h2 style=\"text-align:center\"> Coins</h2>\n    </div>\n    <div class=\"col-sm-12\">\n      <table class=\"table-sm table-hover table-striped table-click\">\n        <thead>\n        <tr>\n          <th>Id</th>\n          <th>Name</th>\n          <th>Value</th>\n          <th>Count</th>\n          <th>Inserted Count</th>\n        </tr>\n        </thead>\n        <tbody>\n        <tr *ngFor=\"let coin of coins; let i=index\">\n          <td>{{coin.id}}</td>\n          <td>{{coin.name}}</td>\n          <td>{{coin.value}}</td>\n          <td>{{coin.count}}</td>\n          <td><input #coinadd type=\"number\" min=\"0\" value=\"0\" class=\"form-control-xs\" style=\"width:5em\">\n            <button class=\"btn btn-info btn-sm\" (click) = 'calculateTenderAmount(i, coin.value, coinadd.value)'>Add</button>\n            <!--<button class=\"badge badge-pill badge-info \" (click) = 'calculateTenderAmount(i, coin.value, coinadd.value)'>Add</button>-->\n          </td>\n        </tr>\n        </tbody>\n      </table>\n    </div>\n    <div class=\"card-footer\">\n      <div>Tendered Amount <span class=\"badge badge-pill badge-info \">R {{vmModel.tenderAmount||0.00}}</span>  </div>\n      <button class=\"btn btn-outline-info btn-sm pull-right\"\n              (click)=\"refreshCoins\">\n        <span>Refresh Coin List</span>\n        <img *ngIf=\"coinRepository.loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\" />\n      </button>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/coins/coins.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model_coin_repository__ = __webpack_require__("../../../../../src/app/model/coin.repository.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_vending_machine_model__ = __webpack_require__("../../../../../src/app/model/vending-machine.model.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoinsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CoinsComponent = (function () {
    function CoinsComponent(coinRepository, vmModel) {
        this.coinRepository = coinRepository;
        this.vmModel = vmModel;
        this.coins_ = [];
        this._tenderAmount = 0.0;
    }
    Object.defineProperty(CoinsComponent.prototype, "coins", {
        get: function () {
            // this.coins_ = this.coinRepository.reloadCoins();
            this.coins_ = this.coinRepository.getCoins();
            return this.coinRepository.getCoins();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CoinsComponent.prototype, "refreshCoins", {
        get: function () {
            this.vmModel.removeMessage();
            this.tenderAmount = 0.0;
            this.vmModel.coins = [];
            this.vmModel.tenderAmount = 0.00;
            return this.coinRepository.reloadCoins();
        },
        enumerable: true,
        configurable: true
    });
    CoinsComponent.prototype.calculateTenderAmount = function (index, coin, coincount) {
        // change state to NEW from any any other state
        this.vmModel.removeMessage();
        if (coincount > 0) {
            this.vmModel.transitionToNewState();
            var i = void 0;
            console.log('Index: ' + index);
            console.log('Coin details: ' + coin);
            console.log('Coins added: ' + coincount);
            console.log('coins_ details: ' + this.coins_);
            this.coins_[index].count = coincount;
            this.totalAmount = 0.0;
            for (i = 0; i <= (this.coins_.length - 1); i++) {
                this.totalAmount = (this.coins_[i].value * this.coins_[i].count) + this.totalAmount;
            }
            this.tenderAmount = this.totalAmount;
            this.vmModel.tenderAmount = this.tenderAmount;
            this.vmModel.coins = this.coins_;
            this.vmModel.selectedProduct = [];
            this.vmModel.transitionToInProcessState();
            console.log('_tenderAmount: ' + this._tenderAmount);
        }
        else {
            this.vmModel.addMessage('Cannot add zero coins, please increase inserted count!');
        }
    };
    Object.defineProperty(CoinsComponent.prototype, "tenderAmount", {
        get: function () {
            return this._tenderAmount;
        },
        set: function (value) {
            this._tenderAmount = value;
        },
        enumerable: true,
        configurable: true
    });
    CoinsComponent.prototype.ngOnInit = function () {
        this.coinRepository.loading = false;
    };
    return CoinsComponent;
}());
CoinsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-coin-list',
        template: __webpack_require__("../../../../../src/app/coins/coins.component.html"),
        styles: [__webpack_require__("../../../../../src/app/coins/coins.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__model_coin_repository__["a" /* CoinRepository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__model_coin_repository__["a" /* CoinRepository */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__model_vending_machine_model__["a" /* VendingMachineModel */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__model_vending_machine_model__["a" /* VendingMachineModel */]) === "function" && _b || Object])
], CoinsComponent);

var _a, _b;
//# sourceMappingURL=coins.component.js.map

/***/ }),

/***/ "../../../../../src/app/message/errorHandler.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__message_service__ = __webpack_require__("../../../../../src/app/message/message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__message_model__ = __webpack_require__("../../../../../src/app/message/message.model.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageErrorHandler; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MessageErrorHandler = (function () {
    function MessageErrorHandler(messageService) {
        this.messageService = messageService;
    }
    MessageErrorHandler.prototype.handleError = function (error) {
        var _this = this;
        var msg = error instanceof Error ? error.message : error.toString();
        setTimeout(function () { return _this.messageService
            .reportMessage(new __WEBPACK_IMPORTED_MODULE_2__message_model__["a" /* Message */](msg, true)); }, 0);
    };
    return MessageErrorHandler;
}());
MessageErrorHandler = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__message_service__["a" /* MessageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__message_service__["a" /* MessageService */]) === "function" && _a || Object])
], MessageErrorHandler);

var _a;
//# sourceMappingURL=errorHandler.js.map

/***/ }),

/***/ "../../../../../src/app/message/message.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"lastMessage\"\n     class=\"bg-info p-a-1 text-xs-center\"\n     [class.bg-danger]=\"lastMessage.error\">\n  <h4>{{lastMessage.text}}</h4>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/message/message.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__message_service__ = __webpack_require__("../../../../../src/app/message/message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_filter__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MessageComponent = (function () {
    function MessageComponent(messageService, router) {
        var _this = this;
        messageService.registerMessageHandler(function (m) { return _this.lastMessage = m; });
        router.events
            .filter(function (e) { return e instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* NavigationEnd */] || e instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* NavigationCancel */]; })
            .subscribe(function (e) { _this.lastMessage = null; });
    }
    return MessageComponent;
}());
MessageComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-messages',
        template: __webpack_require__("../../../../../src/app/message/message.component.html"),
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__message_service__["a" /* MessageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__message_service__["a" /* MessageService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* Router */]) === "function" && _b || Object])
], MessageComponent);

var _a, _b;
//# sourceMappingURL=message.component.js.map

/***/ }),

/***/ "../../../../../src/app/message/message.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Message; });
var Message = (function () {
    function Message(text, error) {
        if (error === void 0) { error = false; }
        this.text = text;
        this.error = error;
    }
    return Message;
}());

//# sourceMappingURL=message.model.js.map

/***/ }),

/***/ "../../../../../src/app/message/message.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__message_component__ = __webpack_require__("../../../../../src/app/message/message.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__message_service__ = __webpack_require__("../../../../../src/app/message/message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__errorHandler__ = __webpack_require__("../../../../../src/app/message/errorHandler.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var MessageModule = (function () {
    function MessageModule() {
    }
    return MessageModule;
}());
MessageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["BrowserModule"], __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* RouterModule */]],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__message_component__["a" /* MessageComponent */]],
        // declarations: [],
        // exports: [],
        exports: [__WEBPACK_IMPORTED_MODULE_2__message_component__["a" /* MessageComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_3__message_service__["a" /* MessageService */],
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_4__errorHandler__["a" /* MessageErrorHandler */] }]
    })
], MessageModule);

//# sourceMappingURL=message.module.js.map

/***/ }),

/***/ "../../../../../src/app/message/message.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MessageService = (function () {
    function MessageService() {
    }
    MessageService.prototype.reportMessage = function (msg) {
        if (this.handler != null) {
            this.handler(msg);
        }
    };
    MessageService.prototype.registerMessageHandler = function (handler) {
        this.handler = handler;
    };
    return MessageService;
}());
MessageService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], MessageService);

//# sourceMappingURL=message.service.js.map

/***/ }),

/***/ "../../../../../src/app/model/coin.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Coin; });
var Coin = (function () {
    function Coin(id, name, value, count) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.count = count;
    }
    return Coin;
}());

//# sourceMappingURL=coin.model.js.map

/***/ }),

/***/ "../../../../../src/app/model/coin.repository.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__static_datasource__ = __webpack_require__("../../../../../src/app/model/static.datasource.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoinRepository; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { RestDataSource } from './rest.datasource';
var CoinRepository = (function () {
    function CoinRepository(dataSource) {
        this.dataSource = dataSource;
        this.coins = [];
        this.datasource = dataSource;
        this.refreshCoins();
    }
    CoinRepository.prototype.refreshCoins = function () {
        var _this = this;
        this.loading = true;
        console.log('refresh coins ...loading = ' + this.loading);
        this.datasource.getCoins().subscribe(function (data) {
            _this.coins = data;
            _this.loading = false;
            console.log('refresh coins ...loading = ' + _this.loading);
        });
    };
    CoinRepository.prototype.getCoins = function () {
        return this.coins;
    };
    CoinRepository.prototype.reloadCoins = function () {
        this.refreshCoins();
        return (this.coins);
    };
    return CoinRepository;
}());
CoinRepository = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__static_datasource__["a" /* StaticDataSource */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__static_datasource__["a" /* StaticDataSource */]) === "function" && _a || Object])
], CoinRepository);

var _a;
//# sourceMappingURL=coin.repository.js.map

/***/ }),

/***/ "../../../../../src/app/model/model.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__product_repository__ = __webpack_require__("../../../../../src/app/model/product.repository.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__coin_repository__ = __webpack_require__("../../../../../src/app/model/coin.repository.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__static_datasource__ = __webpack_require__("../../../../../src/app/model/static.datasource.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__rest_datasource__ = __webpack_require__("../../../../../src/app/model/rest.datasource.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__vending_machine_model__ = __webpack_require__("../../../../../src/app/model/vending-machine.model.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModelModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ModelModule = (function () {
    function ModelModule() {
    }
    return ModelModule;
}());
ModelModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* HttpModule */]],
        providers: [__WEBPACK_IMPORTED_MODULE_1__product_repository__["a" /* ProductRepository */], __WEBPACK_IMPORTED_MODULE_2__coin_repository__["a" /* CoinRepository */], __WEBPACK_IMPORTED_MODULE_6__vending_machine_model__["a" /* VendingMachineModel */],
            // {provide: StaticDataSource, useClass: StaticDataSource},
            { provide: __WEBPACK_IMPORTED_MODULE_3__static_datasource__["a" /* StaticDataSource */], useClass: __WEBPACK_IMPORTED_MODULE_4__rest_datasource__["a" /* RestDataSource */] },
            { provide: __WEBPACK_IMPORTED_MODULE_4__rest_datasource__["b" /* REST_URL */], useValue: "http://" + location.hostname + ":8080/api/" }]
    })
], ModelModule);

//# sourceMappingURL=model.module.js.map

/***/ }),

/***/ "../../../../../src/app/model/product.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Product; });
var Product = (function () {
    function Product(id, itemName, price, quantity) {
        this.id = id;
        this.itemName = itemName;
        this.price = price;
        this.quantity = quantity;
    }
    return Product;
}());

//# sourceMappingURL=product.model.js.map

/***/ }),

/***/ "../../../../../src/app/model/product.repository.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__static_datasource__ = __webpack_require__("../../../../../src/app/model/static.datasource.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductRepository; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { RestDataSource } from './rest.datasource';
var ProductRepository = (function () {
    function ProductRepository(dataSource) {
        this.dataSource = dataSource;
        this.products = [];
        this.datasource = dataSource;
        this.refreshProducts();
    }
    ProductRepository.prototype.refreshProducts = function () {
        var _this = this;
        this.loading = true;
        console.log('refresh products ...loading = ' + this.loading);
        this.datasource.getProducts().subscribe(function (data) {
            _this.products = data;
            _this.loading = false;
            console.log('refresh products ...loading = ' + _this.loading);
        });
    };
    ProductRepository.prototype.getProducts = function () {
        return this.products;
    };
    ProductRepository.prototype.reloadProducts = function () {
        this.refreshProducts();
        return (this.products);
    };
    return ProductRepository;
}());
ProductRepository = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__static_datasource__["a" /* StaticDataSource */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__static_datasource__["a" /* StaticDataSource */]) === "function" && _a || Object])
], ProductRepository);

var _a;
//# sourceMappingURL=product.repository.js.map

/***/ }),

/***/ "../../../../../src/app/model/rest.datasource.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_model__ = __webpack_require__("../../../../../src/app/model/product.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__coin_model__ = __webpack_require__("../../../../../src/app/model/coin.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_from__ = __webpack_require__("../../../../rxjs/add/observable/from.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_from___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_from__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_throw___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_throw__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return REST_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestDataSource; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};









var REST_URL = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["InjectionToken"]('rest_url');
// export const REST_URL = new OpaqueToken('rest_url');
var RestDataSource = (function () {
    function RestDataSource(http, url) {
        this.http = http;
        this.url = url;
        this.products = [
            new __WEBPACK_IMPORTED_MODULE_2__product_model__["a" /* Product */](1, 'Product 1', 10.50, 100),
            new __WEBPACK_IMPORTED_MODULE_2__product_model__["a" /* Product */](2, 'Product 2', 20.50, 50),
            new __WEBPACK_IMPORTED_MODULE_2__product_model__["a" /* Product */](3, 'Product 3', 20.50, 50)
        ];
        this.coins = [
            new __WEBPACK_IMPORTED_MODULE_3__coin_model__["a" /* Coin */](1, 'Coin 1', 10, 75),
            new __WEBPACK_IMPORTED_MODULE_3__coin_model__["a" /* Coin */](2, 'Coin 2', 20, 50),
            new __WEBPACK_IMPORTED_MODULE_3__coin_model__["a" /* Coin */](3, 'Coin 3', 50, 25)
        ];
        this.url_product_api = url + 'v2/products';
        this.url_coin_api = url + 'v2/coins';
    }
    RestDataSource.prototype.getProducts = function () {
        return this.http.get(this.url_product_api).
            map(function (response) { return response.json(); })
            .catch(this.handleErrorObservable);
    };
    RestDataSource.prototype.getCoins = function () {
        return this.http.get(this.url_coin_api)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw("Network Error: " + error.statusText + " (" + error.status + ")"); });
        // .catch(this.handleErrorObservable);
    };
    RestDataSource.prototype.handleErrorObservable = function (error) {
        console.error(error.message || error);
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error.message || error);
    };
    return RestDataSource;
}());
RestDataSource = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(1, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(REST_URL)),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, String])
], RestDataSource);

var _a;
//# sourceMappingURL=rest.datasource.js.map

/***/ }),

/***/ "../../../../../src/app/model/static.datasource.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__product_model__ = __webpack_require__("../../../../../src/app/model/product.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__coin_model__ = __webpack_require__("../../../../../src/app/model/coin.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_from__ = __webpack_require__("../../../../rxjs/add/observable/from.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_from___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_from__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StaticDataSource; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var StaticDataSource = (function () {
    function StaticDataSource() {
        this.products = [
            new __WEBPACK_IMPORTED_MODULE_1__product_model__["a" /* Product */](1, 'Product 1s', 10.50, 100),
            new __WEBPACK_IMPORTED_MODULE_1__product_model__["a" /* Product */](2, 'Product 2s', 20.50, 50),
            new __WEBPACK_IMPORTED_MODULE_1__product_model__["a" /* Product */](3, 'Product 3s', 20.50, 50)
        ];
        this.coins = [
            new __WEBPACK_IMPORTED_MODULE_2__coin_model__["a" /* Coin */](1, 'Coin 1s', 10, 75),
            new __WEBPACK_IMPORTED_MODULE_2__coin_model__["a" /* Coin */](2, 'Coin 2s', 20, 50),
            new __WEBPACK_IMPORTED_MODULE_2__coin_model__["a" /* Coin */](3, 'Coin 3s', 50, 25)
        ];
    }
    StaticDataSource.prototype.getProducts = function () {
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].from([this.products]);
    };
    StaticDataSource.prototype.getCoins = function () {
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].from([this.coins]);
    };
    return StaticDataSource;
}());
StaticDataSource = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], StaticDataSource);

//# sourceMappingURL=static.datasource.js.map

/***/ }),

/***/ "../../../../../src/app/model/vending-machine.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__product_repository__ = __webpack_require__("../../../../../src/app/model/product.repository.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__coin_repository__ = __webpack_require__("../../../../../src/app/model/coin.repository.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VendingMachineModel; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var VendingMachineModel = (function () {
    function VendingMachineModel(productRepository, coinRepository) {
        this.productRepository = productRepository;
        this.coinRepository = coinRepository;
        this.states = {
            NEW: 0,
            INPROCESS: 1,
            COMPLETE: 2
        };
        this._selectedProduct = [];
        this._tenderAmount = 0.00;
        this._selectedItemAmount = 0.00;
        this._selectedItemQty = 0.00;
        this._selectedItemOriginalQty = 0.00;
        this.state = this.states.NEW;
    }
    Object.defineProperty(VendingMachineModel.prototype, "selectedItemOriginalQty", {
        get: function () {
            return this._selectedItemOriginalQty;
        },
        set: function (value) {
            this._selectedItemOriginalQty = value;
        },
        enumerable: true,
        configurable: true
    });
    VendingMachineModel.prototype.canShowMessage = function () {
        return this._showMessage;
    };
    VendingMachineModel.prototype.getShowMessage = function () {
        return this._message;
    };
    VendingMachineModel.prototype.addMessage = function (value) {
        this.message = value;
        this._showMessage = true;
    };
    VendingMachineModel.prototype.removeMessage = function () {
        this.message = '';
        this._showMessage = false;
    };
    Object.defineProperty(VendingMachineModel.prototype, "message", {
        get: function () {
            return this._message;
        },
        set: function (value) {
            this._message = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VendingMachineModel.prototype, "state", {
        get: function () {
            return this._state;
        },
        set: function (value) {
            this._state = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VendingMachineModel.prototype, "selectedProduct", {
        get: function () {
            return this._selectedProduct;
        },
        set: function (value) {
            console.log('VM state (selectedProduct)' + this.state.toString());
            // this.transitionToEndState();
            // console.log('VM state (selectedProduct)' + this.state.toString())
            this._selectedProduct = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VendingMachineModel.prototype, "selectedItemQty", {
        get: function () {
            return this._selectedItemQty;
        },
        set: function (value) {
            this._selectedItemQty = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VendingMachineModel.prototype, "products", {
        get: function () {
            return this._products;
        },
        set: function (value) {
            this._products = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VendingMachineModel.prototype, "coins", {
        get: function () {
            return this._coins;
        },
        set: function (value) {
            this._coins = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VendingMachineModel.prototype, "tenderAmount", {
        get: function () {
            return this._tenderAmount;
        },
        set: function (value) {
            // this.state = this.states.NEW;
            this._tenderAmount = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VendingMachineModel.prototype, "selectedItemAmount", {
        get: function () {
            return this._selectedItemAmount;
        },
        set: function (value) {
            console.log('VM state (selectedItemAmount)' + this.state.toString());
            // this.transitionToEndState();
            // console.log('VM state (selectedItemAmount)' + this.state.toString())
            this._selectedItemAmount = value;
        },
        enumerable: true,
        configurable: true
    });
    VendingMachineModel.prototype.transitionToEndState = function () {
        if (this.state === this.states.COMPLETE) {
            this.state = this.states.NEW;
        }
        else {
            if (this.state === this.states.NEW) {
                this.state = this.states.INPROCESS;
            }
            else {
                if (this.state === this.states.INPROCESS) {
                    this.state = this.states.COMPLETE;
                }
            }
        }
    };
    VendingMachineModel.prototype.returnChange = function () {
        if (this.canCollectItem()) {
            return (this.tenderAmount >= this.selectedItemAmount);
        }
        else {
            return false;
        }
    };
    VendingMachineModel.prototype.calculateChange = function () {
        console.log('VM state (calculateChange)' + this.state.toString());
        // this.transitionToNewState();
        if (this.insufficientQuantity()) {
            console.log('Refund coins ' + this.tenderAmount);
            return this.tenderAmount;
        }
        else {
            if (this.returnChange()) {
                console.log('Return change ' + (this.tenderAmount - this.selectedItemAmount));
                return (this.tenderAmount - this.selectedItemAmount);
            }
            else {
                console.log('Refund coins ' + this.tenderAmount);
                return this.tenderAmount;
            }
        }
    };
    VendingMachineModel.prototype.insufficientQuantity = function () {
        console.log('VM state (insufficientQuantity)' + this.state.toString());
        if (this.selectedItemOriginalQty <= 0) {
            console.log('Insufficient Quantity ' + this.selectedItemOriginalQty);
        }
        else {
            console.log('Sufficient Quantity ' + this.selectedItemOriginalQty);
        }
        // this.transitionToNewState();
        return (this.selectedItemOriginalQty <= 0);
    };
    VendingMachineModel.prototype.insufficientTenderAmount = function () {
        console.log('VM state (insufficientTenderAmount)' + this.state.toString());
        if (this.tenderAmount < this.selectedItemAmount) {
            console.log('Insufficient Tender Amount ' + this.tenderAmount);
        }
        else {
            console.log('Sufficient  Tender Amount ' + this.tenderAmount);
        }
        // this.transitionToNewState();
        return (this.tenderAmount < this.selectedItemAmount);
    };
    // Refactor to using patterns for state machine
    VendingMachineModel.prototype.transitionToNewState = function () {
        this.state = this.states.NEW;
        this.initialiseVars();
    };
    VendingMachineModel.prototype.transitionToInProcessState = function () {
        this.state = this.states.INPROCESS;
    };
    VendingMachineModel.prototype.transitionToCompleteState = function () {
        this.state = this.states.COMPLETE;
    };
    VendingMachineModel.prototype.canCollectItem = function () {
        return ((this.state === this.states.COMPLETE) && (!this.insufficientQuantity()));
    };
    VendingMachineModel.prototype.isCompleted = function () {
        return ((this.state === this.states.COMPLETE));
    };
    VendingMachineModel.prototype.refreshAllData = function () {
        this._products = [];
        this._coins = [];
        this.removeMessage();
        this.transitionToNewState();
        this.productRepository.reloadProducts();
        this.coinRepository.reloadCoins();
    };
    VendingMachineModel.prototype.initialiseVars = function () {
        this._selectedProduct = [];
        this._tenderAmount = 0.00;
        this._selectedItemAmount = 0.00;
        this._selectedItemQty = 0.00;
    };
    return VendingMachineModel;
}());
VendingMachineModel = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__product_repository__["a" /* ProductRepository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__product_repository__["a" /* ProductRepository */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__coin_repository__["a" /* CoinRepository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__coin_repository__["a" /* CoinRepository */]) === "function" && _b || Object])
], VendingMachineModel);

var _a, _b;
//# sourceMappingURL=vending-machine.model.js.map

/***/ }),

/***/ "../../../../../src/app/products/products.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/products/products.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-default\">\n  <div class=\"card-block\">\n    <div class=\"bg-success p-a-1\">\n      <h2 style=\"text-align:center\"> Products</h2>\n    </div>\n\n    <div class=\"col-sm-12\">\n      <table class=\"table-sm table-hover table-striped table-click\">\n        <thead>\n        <tr>\n          <th> Id</th>\n          <th> Item</th>\n          <th> Price (R)</th>\n          <th> Quantity</th>\n          <th> Choice</th>\n        </tr>\n        </thead>\n        <tbody>\n        <tr *ngFor=\"let product of products\" >\n          <td>{{product.id}}</td>\n          <td>{{product.itemName}}</td>\n          <td>{{product.price}}</td>\n          <td>{{product.quantity}}</td>\n          <td>\n            <!--<button type=\"submit\" class=\"btn btn-success btn-sm\"> Select</button>-->\n            <button class=\"btn btn-success btn-sm\" (click) = 'selectedItem(product)'>Select</button>\n          </td>\n        </tr>\n        </tbody>\n      </table>\n    </div>\n\n    <div class=\"card-footer\">\n      <div>Selected Item <span class=\"badge badge-pill badge-success \">{{vmModel.selectedProduct.itemName || ''}} R {{vmModel.selectedItemAmount || 0.00}}</span>  </div>\n      <!--<div>Selected Item <span *ngIf=\"(vmModel.state === vmModel.states.NEW)\" class=\"badge badge-pill badge-success \">R {{_selectedItem.price || 0.00}}</span>  </div>-->\n      <button class=\"btn btn-outline-success btn-sm pull-right\"\n              (click)=\"refreshProducts\">\n        <span>Refresh Product List</span>\n        <img *ngIf=\"productRepository.loading\"\n             src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\"/>\n      </button>\n    </div>\n  </div>\n  <div class=\"card card-default\">\n  <div class=\"card-block\">\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <div style=\"text-align:center\" class=\"bg-primary p-a-1\">\n          <h2> Message </h2>\n        </div>\n        <div style=\"text-align:center\">\n          <!--<div *ngIf=\"vmModel.canCollectItem() && vmModel.returnChange()\" class=\"badge badge-pill badge-success\">-->\n            <!--<h6>-->\n              <!--Collect Item {{vmModel.selectedProduct.itemName||'(none)'}} - Change = R{{vmModel.calculateChange()}}-->\n            <!--</h6>-->\n          <!--</div>-->\n          <div *ngIf=\"vmModel.canCollectItem() && vmModel.returnChange()\" class=\"badge badge-pill badge-success\">Collect Item {{vmModel.selectedProduct.itemName||'(none)'}} - Change = R{{vmModel.calculateChange()}}</div>\n          <div *ngIf=\"vmModel.canCollectItem() && vmModel.insufficientTenderAmount()\" class=\"badge badge-pill badge-warning\" >Insufficient tender amount, Return coins - Change = R {{vmModel.calculateChange()}}</div>\n          <div *ngIf=\"vmModel.isCompleted() && vmModel.insufficientQuantity()\" class=\"badge badge-pill badge-warning\">Insufficient item quantity, Return coins - Change = R {{vmModel.calculateChange()}}</div>\n          <div *ngIf=\"vmModel.canShowMessage()\" class=\"badge badge-pill badge-danger\" >{{vmModel.getShowMessage()}}</div>\n        </div>\n      </div>\n      <div>\n        <button class=\"btn btn-outline-primary btn-sm pull-right\"\n                (click)=\"vmModel.refreshAllData()\">\n          <span>Refresh All Data</span>\n          <img *ngIf=\"vmModel.productRepository.loading || vmModel.coinRepository.loading\"\n               src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\"/>\n        </button>\n      </div>\n    </div>\n  </div>\n</div>\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/products/products.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model_product_repository__ = __webpack_require__("../../../../../src/app/model/product.repository.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_vending_machine_model__ = __webpack_require__("../../../../../src/app/model/vending-machine.model.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductsComponent = (function () {
    function ProductsComponent(productRepository, vmModel) {
        this.productRepository = productRepository;
        this.vmModel = vmModel;
        this.products_ = [];
        this._selectedItem = [];
    }
    Object.defineProperty(ProductsComponent.prototype, "products", {
        get: function () {
            this.products_ = this.productRepository.getProducts();
            return this.productRepository.getProducts();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProductsComponent.prototype, "refreshProducts", {
        get: function () {
            this.vmModel.removeMessage();
            this._selectedItem = [];
            this.vmModel.products = [];
            this.vmModel.selectedProduct = [];
            this.vmModel.selectedItemAmount = 0.00;
            this.vmModel.selectedItemQty = 0.00;
            return this.productRepository.reloadProducts();
        },
        enumerable: true,
        configurable: true
    });
    ProductsComponent.prototype.selectedItem = function (product) {
        // Should be on INPROCESS
        // IF NEW requires tender amount so wait for state change
        // IF COMPLETE reset to NEW
        this.vmModel.removeMessage();
        if (this.vmModel.state === this.vmModel.states.INPROCESS) {
            this._selectedItem = [];
            this.vmModel.selectedItemOriginalQty = product.quantity;
            if (product.quantity >= 1) {
                product.quantity = product.quantity - 1;
            }
            this._selectedItem = product;
            this.vmModel.selectedItemAmount = this._selectedItem.price;
            this.vmModel.selectedItemQty = this._selectedItem.quantity;
            this.vmModel.products = this.products_;
            this.vmModel.selectedProduct = product;
            this.vmModel.transitionToCompleteState();
        }
        else {
            if (this.vmModel.state === this.vmModel.states.COMPLETE) {
                this.vmModel.transitionToNewState();
                this.vmModel.coinRepository.reloadCoins();
            }
            else {
                if (this.vmModel.state === this.vmModel.states.NEW) { }
                this.vmModel.addMessage('Please add coins before selecting a product!');
            }
        }
    };
    ProductsComponent.prototype.ngOnInit = function () {
    };
    return ProductsComponent;
}());
ProductsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-product-list',
        template: __webpack_require__("../../../../../src/app/products/products.component.html"),
        styles: [__webpack_require__("../../../../../src/app/products/products.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__model_product_repository__["a" /* ProductRepository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__model_product_repository__["a" /* ProductRepository */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__model_vending_machine_model__["a" /* VendingMachineModel */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__model_vending_machine_model__["a" /* VendingMachineModel */]) === "function" && _b || Object])
], ProductsComponent);

var _a, _b;
//# sourceMappingURL=products.component.js.map

/***/ }),

/***/ "../../../../../src/app/vending-machine/vending-machine.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/vending-machine/vending-machine.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid jumbotron\">\n  <div style=\"text-align:center\">\n    <H1 class=\"bg-primary bg-inverse text-muted\" >VENDING MACHINE</H1>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-4\">\n    <span>\n      <app-product-list></app-product-list>\n    </span>\n\n    </div>\n    <div class=\"col-md-4\">\n      <app-coin-list></app-coin-list>\n    </div>\n  </div>\n  <div style=\"text-align:center\">\n    <H6 class=\"bg-primary bg-inverse text-muted\" >versions 1.0.0</H6>\n  </div>\n</div>\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/vending-machine/vending-machine.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model_product_repository__ = __webpack_require__("../../../../../src/app/model/product.repository.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_coin_repository__ = __webpack_require__("../../../../../src/app/model/coin.repository.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VendingMachineComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var VendingMachineComponent = (function () {
    function VendingMachineComponent(productRepository, coinRepository) {
        this.productRepository = productRepository;
        this.coinRepository = coinRepository;
    }
    Object.defineProperty(VendingMachineComponent.prototype, "products", {
        get: function () {
            return this.productRepository.getProducts();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VendingMachineComponent.prototype, "coins", {
        get: function () {
            return this.coinRepository.getCoins();
        },
        enumerable: true,
        configurable: true
    });
    VendingMachineComponent.prototype.ngOnInit = function () {
    };
    return VendingMachineComponent;
}());
VendingMachineComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-vending-machine',
        template: __webpack_require__("../../../../../src/app/vending-machine/vending-machine.component.html"),
        styles: [__webpack_require__("../../../../../src/app/vending-machine/vending-machine.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__model_product_repository__["a" /* ProductRepository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__model_product_repository__["a" /* ProductRepository */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__model_coin_repository__["a" /* CoinRepository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__model_coin_repository__["a" /* CoinRepository */]) === "function" && _b || Object])
], VendingMachineComponent);

var _a, _b;
//# sourceMappingURL=vending-machine.component.js.map

/***/ }),

/***/ "../../../../../src/app/vending-machine/vending-machine.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_model_module__ = __webpack_require__("../../../../../src/app/model/model.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__vending_machine_component__ = __webpack_require__("../../../../../src/app/vending-machine/vending-machine.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__coins_coins_component__ = __webpack_require__("../../../../../src/app/coins/coins.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__products_products_component__ = __webpack_require__("../../../../../src/app/products/products.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VendingMachineModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var VendingMachineModule = (function () {
    function VendingMachineModule() {
    }
    return VendingMachineModule;
}());
VendingMachineModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_3__model_model_module__["a" /* ModelModule */], __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["BrowserModule"], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */]],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__vending_machine_component__["a" /* VendingMachineComponent */],
            __WEBPACK_IMPORTED_MODULE_5__coins_coins_component__["a" /* CoinsComponent */],
            __WEBPACK_IMPORTED_MODULE_6__products_products_component__["a" /* ProductsComponent */]
        ],
        exports: [__WEBPACK_IMPORTED_MODULE_4__vending_machine_component__["a" /* VendingMachineComponent */]]
    })
], VendingMachineModule);

//# sourceMappingURL=vending-machine.module.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_notifications__ = __webpack_require__("../../../../angular2-notifications/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_notifications___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_notifications__);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map