package za.co.javadeveloper.vendingmachine.application.domain;

import java.util.List;

public interface VendingMachine {
    public List<Denomination> getDenominations();

    public List<Product> getProducts();
}
