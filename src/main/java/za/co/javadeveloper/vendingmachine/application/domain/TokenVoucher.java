package za.co.javadeveloper.vendingmachine.application.domain;

import java.math.BigDecimal;

public class TokenVoucher implements Denomination /**, Voucher **/
{
    private Long id;
    private String name;
    private BigDecimal value;
    private Integer count;

    public TokenVoucher(Long id, String name, BigDecimal value, Integer count) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.count = count;
    }

    @Override
    public Long getId() {

        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getValue() {
        return value;
    }

    @Override
    public Integer getCount() {
        return count;
    }

    @Override
    public void setCount(Integer count) {

    }
}
