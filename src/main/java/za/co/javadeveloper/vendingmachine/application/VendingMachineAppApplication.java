package za.co.javadeveloper.vendingmachine.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class VendingMachineAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(VendingMachineAppApplication.class, args);
	}
}
