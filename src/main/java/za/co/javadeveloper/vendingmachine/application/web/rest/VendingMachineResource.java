package za.co.javadeveloper.vendingmachine.application.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.javadeveloper.vendingmachine.application.domain.Denomination;
import za.co.javadeveloper.vendingmachine.application.domain.Product;
import za.co.javadeveloper.vendingmachine.application.domain.VendingMachine;

import java.util.List;


@RestController
@RequestMapping("/api")
public class VendingMachineResource {

    private final VendingMachine vendingMachineService;

    public VendingMachineResource(VendingMachine vendingMachineService) {
        this.vendingMachineService = vendingMachineService;
    }

    @GetMapping("/hello")
    public String hello() {
        return "Hello From Spring Boot";
    }
//
//    @GetMapping("/vender/products")
//    public List<Product> products() {
//        List<Product> products = vendingMachineService.getProducts();
//        return products;
//    }
//
//    @GetMapping("/vender/coins")
//    public List<Denomination> coins() {
//        List<Denomination> coins = vendingMachineService.getDenominations();
//        return coins;
//    }

    @GetMapping("/v2/products")
    public ResponseEntity<?> productsV2() {
        List<Product> products = vendingMachineService.getProducts();
        if (products == null){
            return new ResponseEntity<>(products, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(products, HttpStatus.OK);
        }
    }

    @GetMapping("/v2/coins")
    public ResponseEntity<?> coinsV2() {
        List<Denomination> coins = vendingMachineService.getDenominations();
        if (coins == null){
            return new ResponseEntity<>(coins, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(coins, HttpStatus.OK);
        }
    }

}