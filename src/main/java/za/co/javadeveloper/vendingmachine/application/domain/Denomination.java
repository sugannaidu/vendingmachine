package za.co.javadeveloper.vendingmachine.application.domain;

import java.math.BigDecimal;

public interface Denomination {
    Long getId();

    String getName();

    BigDecimal getValue();

    Integer getCount();

    void setCount(Integer count);

}
