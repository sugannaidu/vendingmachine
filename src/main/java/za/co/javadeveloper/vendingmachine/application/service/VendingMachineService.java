package za.co.javadeveloper.vendingmachine.application.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import za.co.javadeveloper.vendingmachine.application.domain.Coins;
import za.co.javadeveloper.vendingmachine.application.domain.Denomination;
import za.co.javadeveloper.vendingmachine.application.domain.Product;
import za.co.javadeveloper.vendingmachine.application.domain.VendingMachine;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
//Todo: create persistance layer

@Service
public class VendingMachineService implements VendingMachine {

    public VendingMachineService() {

    }

    @Override
    @Cacheable("denominations")
    public List<Denomination> getDenominations() {
        List<Denomination> coins = new ArrayList<Denomination>();
        coins.add(new Coins(1l, "50c", new BigDecimal(0.50), 0));
        coins.add(new Coins(2l, "R1", new BigDecimal(1.00), 0));
        coins.add(new Coins(3l, "R2", new BigDecimal(2.00), 0));
        coins.add(new Coins(4l, "R5", new BigDecimal(5.00), 0));
        return coins;
    }

    @Override
    @Cacheable("products")
    public List<Product> getProducts() {
        List<Product> products = new ArrayList<Product>();
        products.add(new Product(1l, "Coke", new BigDecimal(13.50), 25));
        products.add(new Product(2l, "Fanta", new BigDecimal(14.00), 20));
        products.add(new Product(3l, "Sprite", new BigDecimal(12.50), 15));
        return products;
    }

}
