import {Component, OnInit} from '@angular/core';
import {Product} from '../model/product.model';
import {ProductRepository} from '../model/product.repository';
import {VendingMachineModel} from '../model/vending-machine.model';

@Component({
  selector: 'app-product-list',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {
  products_: Product[] = [];
  _selectedItem: Product = [];

  constructor(public productRepository: ProductRepository, public vmModel: VendingMachineModel) {

  }

  get products(): Product[] {
    this.products_ = this.productRepository.getProducts();
    return this.productRepository.getProducts();
  }

  get refreshProducts(): Product[] {
    this.vmModel.removeMessage();
    this._selectedItem = [];
    this.vmModel.products = [];
    this.vmModel.selectedProduct = [];
    this.vmModel.selectedItemAmount = 0.00;
    this.vmModel.selectedItemQty = 0.00;
    return this.productRepository.reloadProducts();
  }

  selectedItem(product: Product) {
    // Should be on INPROCESS
    // IF NEW requires tender amount so wait for state change
    // IF COMPLETE reset to NEW
    this.vmModel.removeMessage();
    if (this.vmModel.state === this.vmModel.states.INPROCESS) {

      this._selectedItem = [];
      this.vmModel.selectedItemOriginalQty = product.quantity;
      if (product.quantity >= 1) {
        product.quantity = product.quantity - 1;
      }
      this._selectedItem = product;
      this.vmModel.selectedItemAmount = this._selectedItem.price;
      this.vmModel.selectedItemQty = this._selectedItem.quantity;
      this.vmModel.products = this.products_;
      this.vmModel.selectedProduct = product;
      this.vmModel.transitionToCompleteState();

    } else {
      if (this.vmModel.state === this.vmModel.states.COMPLETE) {
        this.vmModel.transitionToNewState();
        this.vmModel.coinRepository.reloadCoins();
      } else {
        if (this.vmModel.state === this.vmModel.states.NEW) {}
          this.vmModel.addMessage('Please add coins before selecting a product!');
      }
    }
  }

  ngOnInit() {
  }

}
