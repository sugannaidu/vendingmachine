import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {VendingMachineModule} from './vending-machine/vending-machine.module';
import { MessageComponent } from './message/message.component';
import { MessageModule } from './message/message.module';
import {RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    VendingMachineModule,
    MessageModule,
    RouterModule.forRoot([
      { path: '', component: AppComponent },
      { path: '**', redirectTo: '/' }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent, MessageComponent]
})
export class AppModule {
}
