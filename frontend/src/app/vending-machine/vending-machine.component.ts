import {Component, OnInit} from '@angular/core';
import {Product} from '../model/product.model';
import {ProductRepository} from '../model/product.repository';
import {Coin} from '../model/coin.model';
import {CoinRepository} from '../model/coin.repository';


@Component({
  selector: 'app-vending-machine',
  templateUrl: './vending-machine.component.html',
  styleUrls: ['./vending-machine.component.css']
})
export class VendingMachineComponent implements OnInit {

  constructor(private productRepository: ProductRepository, private coinRepository: CoinRepository) {

  }

  get products(): Product[] {
    return this.productRepository.getProducts();
  }

  get coins(): Coin[] {
    return this.coinRepository.getCoins();
  }



  ngOnInit() {
      }

}
