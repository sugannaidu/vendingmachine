import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {ModelModule} from '../model/model.module';
import {VendingMachineComponent} from './vending-machine.component';
import { CoinsComponent } from '../coins/coins.component';
import { ProductsComponent } from '../products/products.component';

@NgModule({
  imports: [ModelModule, BrowserModule, FormsModule],
  declarations: [
    VendingMachineComponent,
    CoinsComponent,
    ProductsComponent],
  exports: [VendingMachineComponent]
})
export class VendingMachineModule {
}
