import { Injectable } from '@angular/core';
import { Coin } from './coin.model';
import { StaticDataSource } from './static.datasource';
// import { RestDataSource } from './rest.datasource';

@Injectable()
export class CoinRepository {
  private coins: Coin[] = [];
  private datasource: StaticDataSource
  loading: boolean;

  constructor(private dataSource: StaticDataSource) {
    this.datasource = dataSource;
    this.refreshCoins();
  }

  private refreshCoins() {
    this.loading = true;
    console.log('refresh coins ...loading = ' + this.loading);
    this.datasource.getCoins().subscribe(data => {
      this.coins = data;
      this.loading = false;
      console.log('refresh coins ...loading = ' + this.loading);
    });
  }

  getCoins(): Coin[] {
    return this.coins;
  }

  reloadCoins(): Coin[] {
    this.refreshCoins();
    return (this.coins);
  }
}
