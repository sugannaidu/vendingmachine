import {NgModule} from '@angular/core';
import {ProductRepository} from './product.repository';
import {CoinRepository} from './coin.repository';
import {StaticDataSource} from './static.datasource';
import {RestDataSource, REST_URL} from './rest.datasource';
import {HttpModule} from '@angular/http';
import {VendingMachineModel} from './vending-machine.model';

@NgModule({
  imports: [HttpModule],
  providers: [ProductRepository, CoinRepository, VendingMachineModel,
    // {provide: StaticDataSource, useClass: StaticDataSource},
    {provide: StaticDataSource, useClass: RestDataSource},  // , useClass: StaticDataSource]
    { provide: REST_URL, useValue: `http://${location.hostname}:8080/api/`}]
})
export class ModelModule {

}

