import {Product} from './product.model';
import {ProductRepository} from './product.repository';
import {Coin} from './coin.model';
import {CoinRepository} from './coin.repository';
import {Injectable} from '@angular/core';

@Injectable()
export class VendingMachineModel {
  get selectedItemOriginalQty(): number {
    return this._selectedItemOriginalQty;
  }

  set selectedItemOriginalQty(value: number) {
    this._selectedItemOriginalQty = value;
  }
  public canShowMessage(): boolean {
    return this._showMessage;
  }

  public getShowMessage(): string {
    return this._message;
  }

  public addMessage(value: string) {
    this.message = value;
    this._showMessage = true;
  }

  public removeMessage() {
    this.message = '';
    this._showMessage = false;
  }

  get message(): string {
    return this._message;
  }

  set message(value: string) {
    this._message = value;
  }

  public states = {
    NEW: 0,
    INPROCESS: 1,
    COMPLETE: 2
  };

  get state(): number {
    return this._state;
  }

  set state(value: number) {
    this._state = value;
  }

  get selectedProduct(): Product {
    return this._selectedProduct;
  }

  set selectedProduct(value: Product) {
    console.log('VM state (selectedProduct)' + this.state.toString())
    // this.transitionToEndState();
    // console.log('VM state (selectedProduct)' + this.state.toString())
    this._selectedProduct = value;
  }

  get selectedItemQty(): number {
    return this._selectedItemQty;
  }

  set selectedItemQty(value: number) {
    this._selectedItemQty = value;
  }

  get products(): Product[] {
    return this._products;
  }

  set products(value: Product[]) {
    this._products = value;
  }

  get coins(): Coin[] {
    return this._coins;
  }

  set coins(value: Coin[]) {
    this._coins = value;
  }

  get tenderAmount(): number {
    return this._tenderAmount;
  }

  set tenderAmount(value: number) {
    // this.state = this.states.NEW;
    this._tenderAmount = value;
  }

  get selectedItemAmount(): number {
    return this._selectedItemAmount;
  }

  set selectedItemAmount(value: number) {
    console.log('VM state (selectedItemAmount)' + this.state.toString())
    // this.transitionToEndState();
    // console.log('VM state (selectedItemAmount)' + this.state.toString())
    this._selectedItemAmount = value;
  }

  private transitionToEndState() {
    if (this.state === this.states.COMPLETE) {
      this.state = this.states.NEW;
    } else {
      if (this.state === this.states.NEW) {
        this.state = this.states.INPROCESS;
      } else {
        if (this.state === this.states.INPROCESS) {
          this.state = this.states.COMPLETE;
        }
      }
    }
  }

  returnChange(): boolean {
    if (this.canCollectItem()) {
      return (this.tenderAmount >= this.selectedItemAmount);
    } else {
      return false;
    }
  }

  public calculateChange(): number {
    console.log('VM state (calculateChange)' + this.state.toString())
    // this.transitionToNewState();
    if (this.insufficientQuantity()) {
      console.log('Refund coins ' + this.tenderAmount);
      return this.tenderAmount;
    } else {

      if (this.returnChange()) {
        console.log('Return change ' + (this.tenderAmount - this.selectedItemAmount));
        return (this.tenderAmount - this.selectedItemAmount);
      } else {
        console.log('Refund coins ' + this.tenderAmount);
        return this.tenderAmount;
      }
    }
  }

  insufficientQuantity(): boolean {
    console.log('VM state (insufficientQuantity)' + this.state.toString())
    if (this.selectedItemOriginalQty <= 0) {
      console.log('Insufficient Quantity ' + this.selectedItemOriginalQty);
    } else {
      console.log('Sufficient Quantity ' + this.selectedItemOriginalQty);
    }
    // this.transitionToNewState();
    return (this.selectedItemOriginalQty <= 0);
  }

  insufficientTenderAmount(): boolean {
    console.log('VM state (insufficientTenderAmount)' + this.state.toString())
    if (this.tenderAmount < this.selectedItemAmount) {
      console.log('Insufficient Tender Amount ' + this.tenderAmount);
    } else {
      console.log('Sufficient  Tender Amount ' + this.tenderAmount);
    }
    // this.transitionToNewState();
    return (this.tenderAmount < this.selectedItemAmount);
  }

  // Refactor to using patterns for state machine
  public transitionToNewState() {
    this.state = this.states.NEW;
    this.initialiseVars();
  }

  public transitionToInProcessState() {
      this.state = this.states.INPROCESS;
  }

  public transitionToCompleteState() {
      this.state = this.states.COMPLETE;
  }

  canCollectItem(): boolean {
    return ((this.state === this.states.COMPLETE) && (!this.insufficientQuantity()));
  }

  isCompleted(): boolean {
    return ((this.state === this.states.COMPLETE));
  }

  refreshAllData() {
    this._products = [];
    this._coins = [];
    this.removeMessage();
    this.transitionToNewState();
    this.productRepository.reloadProducts();
    this.coinRepository.reloadCoins();
  }


  private initialiseVars() {
    this._selectedProduct = [];
    this._tenderAmount = 0.00;
    this._selectedItemAmount = 0.00;
    this._selectedItemQty = 0.00;
  }

  private _products: Product[];
  private _selectedProduct: Product = [];
  private _coins: Coin[];
  private _tenderAmount: number = 0.00;
  private _selectedItemAmount: number = 0.00;
  private _selectedItemQty: number = 0.00;
  private _selectedItemOriginalQty: number = 0.00;
  private _state: number;
  private _message: string;
  private _showMessage: boolean;

  constructor(public productRepository: ProductRepository, public coinRepository: CoinRepository) {
    this.state = this.states.NEW;
  }
}
