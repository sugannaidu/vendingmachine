import {Injectable} from '@angular/core';
import {Product} from './product.model';
import {Coin} from './coin.model';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/from';


@Injectable()
export class StaticDataSource {
  private products: Product[] = [
    new Product(1, 'Product 1s', 10.50, 100),
    new Product(2, 'Product 2s', 20.50, 50),
    new Product(3, 'Product 3s', 20.50, 50)
  ];

  private coins: Coin[] = [
    new Coin(1, 'Coin 1s', 10, 75),
    new Coin(2, 'Coin 2s', 20, 50),
    new Coin(3, 'Coin 3s', 50, 25)
  ];

  getProducts(): Observable<Product[]> {
    return Observable.from([this.products]);
  }

  getCoins(): Observable<Coin[]> {
    return Observable.from([this.coins]);
  }
}


