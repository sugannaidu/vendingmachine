import { Injectable } from '@angular/core';
import { Product } from './product.model';
import { StaticDataSource } from './static.datasource';
// import { RestDataSource } from './rest.datasource';

@Injectable()
export class ProductRepository {
  private products: Product[] = [];
  private datasource: StaticDataSource
  loading: boolean;

  constructor(private dataSource: StaticDataSource) {
    this.datasource = dataSource;
    this.refreshProducts();
  }

  private refreshProducts() {
    this.loading = true;
    console.log('refresh products ...loading = ' + this.loading);
    this.datasource.getProducts().subscribe(data => {
      this.products = data;
      this.loading = false;
      console.log('refresh products ...loading = ' + this.loading);
    });
  }

  getProducts(): Product[] {
    return this.products;
  }

  reloadProducts(): Product[] {
    this.refreshProducts();
    return (this.products);
  }

}
