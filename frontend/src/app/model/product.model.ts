export class Product {
  constructor(public id?: number,
              public itemName?: string,
              public price?: number,
              public quantity?: number) {
  }
}
