export class Coin {
  constructor(public id?: number,
              public name?: string,
              public value?: number,
              public count?: number) {
  }
}
