import {Inject, Injectable, InjectionToken, OpaqueToken} from '@angular/core';
import {Http, RequestMethod} from '@angular/http';
import {Product} from './product.model';
import {Coin} from './coin.model';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

export const REST_URL = new InjectionToken<String>('rest_url');
// export const REST_URL = new OpaqueToken('rest_url');


@Injectable()
export class RestDataSource {
  private products: Product[] = [
    new Product(1, 'Product 1', 10.50, 100),
    new Product(2, 'Product 2', 20.50, 50),
    new Product(3, 'Product 3', 20.50, 50)
  ];

  private coins: Coin[] = [
    new Coin(1, 'Coin 1', 10, 75),
    new Coin(2, 'Coin 2', 20, 50),
    new Coin(3, 'Coin 3', 50, 25)
  ];

  private url_product_api: string
  private url_coin_api: string

  constructor(private http: Http,
              @Inject(REST_URL) private url: string) {
    this.url_product_api = url + 'v2/products';
    this.url_coin_api = url + 'v2/coins';
  }

  getProducts(): Observable<Product[]> {
    return this.http.get(this.url_product_api).
    map(response => response.json())
      .catch(this.handleErrorObservable);
  }

  getCoins(): Observable<Coin[]> {
    return this.http.get(this.url_coin_api)
      .map(response => response.json())
      .catch((error: Response) => Observable.throw(
        `Network Error: ${error.statusText} (${error.status})`)
      );
      // .catch(this.handleErrorObservable);

  }

  private handleErrorObservable (error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

}
