import {Component, OnInit} from '@angular/core';
import {Coin} from '../model/coin.model';
import {CoinRepository} from '../model/coin.repository';
import {VendingMachineModel} from '../model/vending-machine.model';

@Component({
  selector: 'app-coin-list',
  templateUrl: './coins.component.html',
  styleUrls: ['./coins.component.css']
})
export class CoinsComponent implements OnInit {

  coins_: Coin[] = [];

  constructor(public coinRepository: CoinRepository, public vmModel: VendingMachineModel) {

  }

  get coins(): Coin[] {
    // this.coins_ = this.coinRepository.reloadCoins();
    this.coins_ = this.coinRepository.getCoins();
    return this.coinRepository.getCoins();
  }

  get refreshCoins(): Coin[] {
    this.vmModel.removeMessage();
    this.tenderAmount = 0.0;
    this.vmModel.coins = [];
    this.vmModel.tenderAmount = 0.00;
    return this.coinRepository.reloadCoins();
  }

  protected _tenderAmount = 0.0;
  protected totalAmount: number;

  calculateTenderAmount(index: number, coin: number, coincount: number) {
    // change state to NEW from any any other state
    this.vmModel.removeMessage();
    if (coincount > 0) {
      this.vmModel.transitionToNewState();
      let i: number;
      console.log('Index: ' + index);
      console.log('Coin details: ' + coin);
      console.log('Coins added: ' + coincount);
      console.log('coins_ details: ' + this.coins_);
      this.coins_[index].count = coincount;
      this.totalAmount = 0.0;
      for (i = 0; i <= (this.coins_.length - 1); i++) {
        this.totalAmount = (this.coins_[i].value * this.coins_[i].count) + this.totalAmount;
      }
      this.tenderAmount = this.totalAmount;
      this.vmModel.tenderAmount = this.tenderAmount;
      this.vmModel.coins = this.coins_;
      this.vmModel.selectedProduct = [];
      this.vmModel.transitionToInProcessState();
      console.log('_tenderAmount: ' + this._tenderAmount);
    } else {
      this.vmModel.addMessage('Cannot add zero coins, please increase inserted count!');
    }

  }

  get tenderAmount() {
    return this._tenderAmount;
  }

  set tenderAmount(value) {
    this._tenderAmount = value;
  }

  ngOnInit() {
    this.coinRepository.loading = false;
  }

}
