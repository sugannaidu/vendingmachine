import { VendingMachineClientPage } from './app.po';

describe('vending-machine-client App', () => {
  let page: VendingMachineClientPage;

  beforeEach(() => {
    page = new VendingMachineClientPage();
  });

  it('should display application name', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Vending Machine');
  });
});
